package main

import (
	"ShaderTest/util"
	"image"
	"image/png"
	"math/rand"
	"os"
	"time"
	"unsafe"

	"github.com/veandco/go-sdl2/sdl"

	"github.com/go-gl/gl/v4.6-core/gl"
)

func randomNoise(w, h int) []uint8 {
	pxls := make([]uint8, w*h*4)
	for i := 0; i < w*h*4; i += 4 {
		pxls[i+0] = uint8(rand.Intn(256))
		pxls[i+1] = uint8(rand.Intn(256))
		pxls[i+2] = uint8(rand.Intn(256))
		pxls[i+3] = 255
	}
	return pxls
}

func loadImage(path string) (*image.NRGBA, error) {
	fileContent, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	imageData, err := png.Decode(fileContent)
	return imageData.(*image.NRGBA), err
}

func main() {
	rand.NewSource(time.Now().UnixNano())

	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}

	var window *sdl.Window
	var renderer *sdl.Renderer

	window, err := sdl.CreateWindow("ShaderTest", 10, 10, 800, 600, sdl.WINDOW_SHOWN)
	if err != nil {
		panic(err)
	}
	defer window.Destroy()

	renderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		panic(err)
	}
	defer renderer.Destroy()

	_, err = window.GLCreateContext()
	if err != nil {
		panic(err)
	}
	err = gl.Init()
	if err != nil {
		panic(err)
	}

	gl.Enable(gl.TEXTURE_2D)
	CreateDebugLog()

	shader := util.Load("./shaders/shader.glsl", gl.COMPUTE_SHADER)
	shaderProgram := util.CreateShaderProgram()
	shaderProgram.Attach(shader)
	shaderProgram.Link()

	gl.UseProgram(shaderProgram.ID)

	//---Output buffer---
	//Generate and bind output image buffer
	var texOut uint32
	gl.GenTextures(1, &texOut)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texOut)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gl.TexImage2D(
		gl.TEXTURE_2D,
		0,
		gl.RGBA8,
		800,
		600,
		0,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		unsafe.Pointer(nil),
	)
	gl.BindImageTexture(0, texOut, 0, false, 0, gl.WRITE_ONLY, gl.RGBA8)

	//Generate and bind frame buffer
	var fBuffer uint32
	gl.GenFramebuffers(1, &fBuffer)
	gl.BindFramebuffer(gl.FRAMEBUFFER, fBuffer)
	gl.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texOut, 0)
	gl.BindFramebuffer(gl.READ_FRAMEBUFFER, fBuffer)
	gl.BindFramebuffer(gl.DRAW_FRAMEBUFFER, 0)

	// Generate and set texture input
	sheet, err := loadImage("geometry.png")
	if err != nil {
		panic(err)
	}

	var texarray uint32

	gl.GenTextures(1, &texarray)
	gl.ActiveTexture(gl.TEXTURE0 + 1)
	gl.BindTexture(gl.TEXTURE_2D_ARRAY, texarray)

	gl.TexParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_MIN_FILTER, gl.LINEAR)

	gl.TexImage3D(
		gl.TEXTURE_2D_ARRAY,
		0,
		gl.RGBA8,
		16,
		16,
		22*48,
		0,
		gl.RGBA, gl.UNSIGNED_BYTE,
		gl.Ptr(sheet.Pix))
	gl.BindImageTexture(1, texarray, 0, false, 0, gl.READ_ONLY, gl.RGBA8)

	running := true

	for running {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch event.(type) {
			case *sdl.QuitEvent:
				running = false
			}
		}

		gl.DispatchCompute(800, 600, 1)
		gl.MemoryBarrier(gl.SHADER_IMAGE_ACCESS_BARRIER_BIT)

		gl.BlitFramebuffer(0, 0, 800, 600, 0, 0, 800, 600, gl.COLOR_BUFFER_BIT, gl.LINEAR)

		window.GLSwap()
	}

	SaveDebugLog()
}
