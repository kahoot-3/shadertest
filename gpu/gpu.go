package gpu

import (
	"github.com/go-gl/gl/v4.6-core/gl"
)

func CreateArrayTexture(width, height, depth int32, pxl []uint8, shader uint32) uint32 {
	var tex uint32

	gl.GenTextures(1, &tex)
	gl.ActiveTexture(gl.TEXTURE0 + 1)
	gl.BindTexture(gl.TEXTURE_2D_ARRAY, tex)

	gl.TexParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_MIN_FILTER, gl.LINEAR)

	gl.TexImage3D(
		gl.TEXTURE_2D_ARRAY,
		0,
		gl.RGBA8,
		width,
		height,
		depth,
		0,
		gl.RGBA, gl.UNSIGNED_BYTE,
		gl.Ptr(pxl))

	return tex
}
