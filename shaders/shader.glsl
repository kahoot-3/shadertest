#version 430
layout(local_size_x = 1, local_size_y = 1) in;
layout(rgba32f, binding = 0) uniform image2D img;

layout(binding = 1) uniform sampler2DArray texAtlas;

void main() {
    ivec2 iCoords = ivec2(gl_GlobalInvocationID.xy);
    vec4 c = texture(texAtlas, vec3(iCoords.x%16, iCoords.y%16, 7));
    imageStore(img, iCoords, c);
}
