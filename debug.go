package main

import (
	"fmt"
	"io/ioutil"
	"unsafe"

	"github.com/go-gl/gl/v4.6-core/gl"
)

//For better readability of OpenGL errors
var GLErrors = map[uint32]string{
	gl.DEBUG_SEVERITY_HIGH:         "HIGH",
	gl.DEBUG_SEVERITY_MEDIUM:       "MEDIUM",
	gl.DEBUG_SEVERITY_LOW:          "LOW",
	gl.DEBUG_SEVERITY_NOTIFICATION: "NOTIFY",
}

var GLMessageTypes = map[uint32]string{
	gl.DEBUG_TYPE_ERROR:               "ERROR",
	gl.DEBUG_TYPE_DEPRECATED_BEHAVIOR: "DEPRECATED_BEHAVIOUR",
	gl.DEBUG_TYPE_UNDEFINED_BEHAVIOR:  "UNDEFINED_BEHAVIOUR",
	gl.DEBUG_TYPE_PORTABILITY:         "PORTABILITY",
	gl.DEBUG_TYPE_PERFORMANCE:         "PERFORMANCE",
	gl.DEBUG_TYPE_MARKER:              "MARKER",
	gl.DEBUG_TYPE_PUSH_GROUP:          "PUSH_GROUP",
	gl.DEBUG_TYPE_POP_GROUP:           "POP_GROUP",
	gl.DEBUG_TYPE_OTHER:               "OTHER",
}

var GLSources = map[uint32]string{
	gl.DEBUG_SOURCE_API:             "API",
	gl.DEBUG_SOURCE_WINDOW_SYSTEM:   "WINDOW_SYSTEM",
	gl.DEBUG_SOURCE_SHADER_COMPILER: "SHADER_COMPILER",
	gl.DEBUG_SOURCE_THIRD_PARTY:     "THIRD_PARTY",
	gl.DEBUG_SOURCE_APPLICATION:     "APPLICATION",
	gl.DEBUG_SOURCE_OTHER:           "OTHER",
}

type debugLog []string

var (
	DebugLog debugLog
)

const (
	GL_DEBUG_LOG_DEFAULT_PATH string = "./gl_log.txt"
)

func CreateDebugLog() {
	DebugLog = make([]string, 0)

	gl.Enable(gl.DEBUG_OUTPUT)
	gl.DebugMessageCallback(glCallback, nil)
}

func glCallback(source uint32, gltype uint32, id uint32, severity uint32, length int32, message string, userParam unsafe.Pointer) {
	DebugLog = append(DebugLog, fmt.Sprintf("GL CALLBACK: source = %s, type = %s, severity = %s, message = %s\n", GLSources[source], GLMessageTypes[gltype], GLErrors[severity], message))
}

func SaveDebugLog() error {
	data := make([]byte, 0)
	for i := range DebugLog {
		data = append(data, []byte(DebugLog[i])...)
	}
	return ioutil.WriteFile(GL_DEBUG_LOG_DEFAULT_PATH, data, 0644)
}
